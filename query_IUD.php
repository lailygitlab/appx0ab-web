<?php
$DB_NAME = "kampus";
$DB_USER = "root";
$DB_PASS = "";
$DB_SERVER_LOC = "localhost";

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $conn = mysqli_connect($DB_SERVER_LOC, $DB_USER, $DB_PASS, $DB_NAME);
    $mode = $_POST['mode'];
    $response = array(); $response['kode'] = '000';
    switch ($mode) {
        case "insert":
            $nim = $_POST['nim'];
            $nama = $_POST['nama'];
            $nama_prodi = $_POST['nama_prodi'];
            $imstr = $_POST['image'];
            $file = $_POST['file'];
            $path = "images/";

            $sql = "select id_prodi from prodi where nama_prodi='$nama_prodi'";
            $result = mysqli_query($conn, $sql);
            if (mysqli_num_rows($result) > 0) {
                $data = mysqli_fetch_assoc($result);
                $id_prodi = $data['id_prodi'];

                $sql = "insert into mahasiswa(nim, nama, id_prodi, photos) values(
                    '$nim','$nama',$id_prodi,'$file'
                )";
                $result = mysqli_query($conn, $sql);
                if ($result) {
                    if (file_put_contents($path.$file, base64_decode($imstr)) == false) {
                        $sql = "delete from mahasiswa where nim = '$nim'";
                        mysqli_query($conn, $sql);
                        $response['kode'] = "111";
                        echo json_encode($response);
                        exit();
                    } else {
                        echo json_encode($response); //insert data sukses semua
                        exit();
                    }
                } else {
                    $response['kode'] = "111";
                    echo json_encode($response);
                    exit();
                }
            }
            break;

        case "update":
            $nim = $_POST['nim'];
            $nama = $_POST['nama'];
            $nama_prodi = $_POST['nama_prodi'];
            $imstr = $_POST['image'];
            $file = $_POST['file'];
            $path = "images/";

            $sql = "select id_prodi from prodi where nama_prodi='$nama_prodi'";
            $result = mysqli_query($conn, $sql);
            if (mysqli_num_rows($result) > 0) {
                $data = mysqli_fetch_assoc($result);
                $id_prodi = $data['id_prodi'];

                $sql = "";
                if ($imstr == "") {
                    $sql = "update mahasiswa set nama='$nama', id_prodi=$id_prodi
                            where nim='$nim'";
                    $result = mysqli_query($conn, $sql);
                    if ($result) {
                        echo json_encode($response); //update data sukses semua
                        exit();
                    } else {
                        $response['kode'] = "111";
                        echo json_encode($response); exit();
                    }
                } else {
                        if (file_put_contents($path . $file, base64_decode($imstr)) == false) {
                            $response['kode'] = "111";
                            echo json_encode($response);
                            exit();
                        } else {
                            $sql = "update mahasiswa set nama='$nama',id_prodi=$id_prodi, photos='$file'
                            where nim='$nim'";
                            $result = mysqli_query($conn, $sql);
                            if ($result) {
                                echo json_encode($response); //update data sukses semua
                                exit();
                            } else {
                                $response['kode'] = "111";
                                echo json_encode($response);
                                exit();
                            }
                        }
                    }
            }
            break;

        case "delete":
            $nim = $_POST['nim'];

            $sql = "select photos from mahasiswa where nim='$nim'";
            $result = mysqli_query($conn, $sql);
                if ($result) {
                    if (mysqli_num_rows($result) > 0) {
                        $data = mysqli_fetch_assoc($result);
                        $photos = $data['photos'];
                        $path = "images/";
                        unlink($path.$photos);
                    }

                    $sql = "delete from mahasiswa where nim='$nim'";
                    $result = mysqli_query($conn, $sql);
                    if ($result) {
                        echo json_encode($response); //delete data sukses semua
                        exit();
                    }  else {
                        $response['kode'] = "111";
                        echo json_encode($response);
                        exit();
                    }
            }
            break;
    }
}
?>
