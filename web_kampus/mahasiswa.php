<?php
include 'koneksi.php';
$db = new database();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">Kampus</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="mahasiswa.php">Mahasiswa <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="prodi.php">Prodi</a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form>
  </div>
</nav>
    <title>Data Mahasiswa</title>
    <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap.min.css" >
</head>
<body>
    <?php
    error_reporting(0);
    $page = $_GET['page'];
    if (empty($page)) :
    ?>
    <div class="container">
    <h3>Data Mahasiswa</h3>
        <table class="table">
            <thead class="thead-dark">
                <tr>
                <th scope="col">NIM</th>
                <th scope="col">Nama</th>
                <th scope="col">Id_Prodi</th>
                <th scope="col">Alamat</th>
                <th scope="col">Photos</th>
                
                </tr>
            </thead>
            <?php
                foreach ($db->tampil_data() as $data){
                    ?>
            <tbody>
                            <tr>
                            <th scope="row"><?php echo $data['nim']; ?></th>
                            <td><?php echo $data['nama']; ?></td>
                            <td><?php echo $data['id_prodi']; ?></td>
                            <td><?php echo $data['alamat']; ?></td>
                            <td><?php echo $data['photos']; ?></td>
                            </tr>
                          <?php  } ?>
                        </tbody>
        </table>
      
     <?php  endif; ?>
</body>
</html>